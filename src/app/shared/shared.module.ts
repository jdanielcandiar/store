import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';


import { ExponentialPipe } from './pipes/exponential/exponential.pipe';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { ProductComponent } from "./components/product/product.component";
import { MaterialModule } from "./../material/material.module";

@NgModule({
    declarations: [
        ExponentialPipe,
        HeaderComponent,
        FooterComponent,
        ProductComponent
    ],
    imports: [
        CommonModule,
        RouterModule,
        MaterialModule
    ],
    exports: [
        HeaderComponent, 
        FooterComponent, 
        ProductComponent
    ]
})

export class SharedModule { }
